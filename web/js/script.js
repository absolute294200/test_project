function search() {

    if($('#search').val() != ''){
        $('.info-row').css('display', 'none');
        $( ".id:contains(" + $('#search').val() + ")" ).parent().css('display', 'block');
        $( ".ua:contains(" + $('#search').val() + ")" ).parent().css('display', 'block');
        $( ".ip:contains(" + $('#search').val() + ")" ).parent().css('display', 'block');
        $( ".ref:contains(" + $('#search').val() + ")" ).parent().css('display', 'block');
        $( ".param1:contains(" + $('#search').val() + ")" ).parent().css('display', 'block');
        $( ".param2:contains(" + $('#search').val() + ")" ).parent().css('display', 'block');
        $( ".error:contains(" + $('#search').val() + ")" ).parent().css('display', 'block');
        $( ".bad_domain:contains(" + $('#search').val() + ")" ).parent().css('display', 'block');
    }else{
        $('.info-row').css('display', 'block');
    }
}

$(document).ready(function()
    {
        $("#myTable").tablesorter();
    }
);