<?php

namespace ApiBundle\Controller;

use CoreBundle\Handler\ClickHandler;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations;

/**
 * Class SuccessController
 *
 */
class SuccessController extends FOSRestController
{

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Success",
     *  description="Check if click info exists and show the click information",
     *  statusCodes={
     *      200 = "Ok",
     *      204 = "Positions not found",
     *      400 = "Bad format",
     *      403 = "Forbidden"
     *  }
     *)
     * @Annotations\Get("/success/{id}")
     * @param Request $request
     *
     * @return Response
     */
    public function getAction(Request $request) : Response
    {
        try{

            /** @var ClickHandler $clickHandler */
            $clickHandler = $this->get('core.handler.click');

            $clickId = $request->get('id');

            $click = $clickHandler->processGet($clickId);

            return $this->render('success.html.twig', [
                'click_id' => $click->getId(),
                'user_agent' => $click->getUa(),
                'referrer' => $click->getRef(),
                'ip' => $click->getIp(),
                'param1' => $click->getParam1(),
                'param2' => $click->getParam2()
            ]);

        }catch (EntityNotFoundException $exception){
            return new Response('Error: Not found', 404);
        }
    }
}