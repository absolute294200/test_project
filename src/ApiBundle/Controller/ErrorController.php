<?php

namespace ApiBundle\Controller;

use CoreBundle\Handler\ClickHandler;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations;

/**
 * Class ErrorController
 *
 * @RouteResource("/error/{id}")
 */
class ErrorController extends FOSRestController
{

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Error",
     *  description="Check if click info exists and show error message",
     *  statusCodes={
     *      200 = "Ok",
     *      204 = "Positions not found",
     *      400 = "Bad format",
     *      403 = "Forbidden"
     *  }
     *)
     * @param Request $request
     *
     * @return Response
     */
    public function getAction(Request $request) : Response
    {
        try{
            /** @var ClickHandler $clickHandler */
            $clickHandler = $this->get('core.handler.click');

            $clickId = $request->get('id');

            $click = $clickHandler->processGet($clickId);

            return $this->render('error.html.twig', [
                'click_id' => $click->getId(),
                'error' => $click->getError(),
                'bad_domain' => $click->isBadDomain() ? 'true' : 'false'
            ]);

        }catch (EntityNotFoundException $exception){
            return new Response('Error: Not found', 404);
        }
    }
}