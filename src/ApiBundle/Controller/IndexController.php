<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations;

/**
 * Class IndexController
 *
 * @RouteResource("/")
 */
class IndexController extends FOSRestController
{

    /**
     * @ApiDoc(
     *  resource=true,
     *  section="Click",
     *  description="Create new Click entity from GET query",
     *  statusCodes={
     *      200 = "Ok",
     *      204 = "Positions not found",
     *      400 = "Bad format",
     *      403 = "Forbidden"
     *  }
     *)
     * @param Request $request
     *
     * @return Response
     */
    public function getAction(Request $request) : Response
    {
        $clickHandler = $this->get('core.handler.click');

        $clicksArray = $clickHandler->processGetAll();

        return $this->render('index.html.twig', ['clicks' => $clicksArray]);
    }
}