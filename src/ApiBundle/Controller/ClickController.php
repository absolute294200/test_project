<?php

namespace ApiBundle\Controller;

use CoreBundle\Entity\Click;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations;
use CoreBundle\Handler\ClickHandler;

/**
 * Class ClickController
 *
 * @RouteResource("Click")
 */
class ClickController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Click",
     *  description="Create new Click entity from GET query",
     *  statusCodes={
     *      200 = "Ok",
     *      204 = "Positions not found",
     *      400 = "Bad format",
     *      403 = "Forbidden"
     *  }
     *)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getAction(Request $request) : Response
    {
        $clickHandler = $this->get('core.handler.click');

        $data['userAgent'] = $request->server->get('HTTP_USER_AGENT');
        $data['clientIp'] = $request->getClientIp();
        $data['param1'] = $request->get('param1');
        $data['param2'] = $request->get('param2');
        $data['referrer'] = is_null($request->server->get('HTTP_REFERER')) ? '/' : $request->server->get('HTTP_REFERER');

        /** @var Click $click */
        $click = $clickHandler->processCreate($data);

        if($click->getError() == 0){
            $redirectTo = $request->getUriForPath(NULL) . '/success';
        }else{
            $redirectTo = $request->getUriForPath(NULL) . '/error';
        }

        return $this->render('redirect.html.twig', ['link' => $redirectTo . '/' . $click->getId()]);
    }
}