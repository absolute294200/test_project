<?php

namespace ApiBundle\Controller;

use CoreBundle\Entity\BadDomain;
use CoreBundle\Entity\Click;
use CoreBundle\Handler\BadDomainHandler;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Form\Exception\AlreadySubmittedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations;
use CoreBundle\Handler\ClickHandler;

/**
 * Class BadDomainController
 *
 * @RouteResource("/referrers")
 */
class BadDomainController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Bad domain",
     *  description="Get list of bad domainsn",
     *  statusCodes={
     *      200 = "Ok",
     *      204 = "Positions not found",
     *      400 = "Bad format",
     *      403 = "Forbidden"
     *  }
     *)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getAction(Request $request) : Response
    {
        /** @var BadDomainHandler $badDomainHandler */
        $badDomainHandler = $this->get('core.handler.bad_domain');

        $badDomainsArray = $badDomainHandler->processGetAll();

        return $this->render('bad_domains.html.twig', ['bad_domains' => $badDomainsArray]);
    }

    /**
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Bad domain",
     *  description="Create new Bad domain",
     *  statusCodes={
     *      200 = "Ok",
     *      204 = "Positions not found",
     *      400 = "Bad format",
     *      403 = "Forbidden"
     *  }
     *)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postAction(Request $request) : Response
    {
        try{
            /** @var BadDomainHandler $badDomainHandler */
            $badDomainHandler = $this->get('core.handler.bad_domain');

            $badDomainsArray = $badDomainHandler->processCreate(['name' => $request->get('name')]);

            return $this->render('bad_domains.html.twig', ['bad_domains' => $badDomainsArray]);
        }catch (AlreadySubmittedException $exception){
            return new Response('Error: already exists domain', 400);
        }
    }
}