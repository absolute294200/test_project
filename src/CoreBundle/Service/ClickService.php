<?php

namespace CoreBundle\Service;

use CoreBundle\Entity\BadDomain;
use CoreBundle\Entity\Click;
use SimpleSymfony\Symfony\RestBundle\Service\AbstractService;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ClickService
 */
class ClickService  extends AbstractService implements ContainerAwareInterface, EventSubscriberInterface
{
    use ContainerAwareTrait;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager|object
     */
    private $manager;

    /**
     * @var BadDomainService
     */
    private $badDomainService;

    /**
     * ClickService constructor.
     * @param ContainerInterface $container
     * @param string $entityClass
     * @param EventDispatcherInterface $eventDispatcher
     * @param BadDomainService $badDomainService
     */
    public function __construct(
        ContainerInterface $container,
        string $entityClass,
        EventDispatcherInterface $eventDispatcher,
        BadDomainService $badDomainService
    ) {
        parent::__construct($container, $entityClass);
        $this->setContainer($container);
        $this->manager = $this->getManager();
        $this->eventDispatcher = $eventDispatcher;
        $this->badDomainService = $badDomainService;
    }

    /**
     * @param array $data
     * @return Click
     */
    public function create(array $data): Click
    {
        $badDomainFlag = FALSE;

        $id = base64_encode(
            $data['userAgent'] .
            $data['clientIp'] .
            $data['param1'] .
            $data['referrer']
        );

        $badDomains = $this->badDomainService->getAll();
        /** @var BadDomain $item */
        foreach ($badDomains as $item){
            if($item->getName() == $data['referrer']){
                $badDomainFlag = TRUE;
                break;
            }
        }

        $clickArray = $this->getEntitiesBy(['id' => $id]);

        if(count($clickArray) == 0){
            $click = new Click();
            $click->setId($id);
            $click->setIp($data['clientIp']);
            $click->setParam1($data['param1']);
            $click->setParam2($data['param2']);
            $click->setRef($data['referrer']);
            $click->setUa($data['userAgent']);
            $click->setError(0);
            $click->setBadDomain(0);
        }else{
            /** @var Click $click */
            $click = $clickArray[0];
            $click->setError($click->getError()+1);
        }

        if($badDomainFlag){
            $click->setError($click->getError()+1);
            $click->setBadDomain(1);
        }

        $this->manager->persist($click);
        $this->manager->flush();

        return $click;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {

        $clicksArray = $this->getEntities();

        return $clicksArray;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [];
    }
}