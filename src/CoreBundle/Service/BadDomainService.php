<?php

namespace CoreBundle\Service;

use CoreBundle\Entity\BadDomain;
use SimpleSymfony\Symfony\RestBundle\Service\AbstractService;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class BadDomainService
 */
class BadDomainService extends AbstractService implements ContainerAwareInterface, EventSubscriberInterface
{
    use ContainerAwareTrait;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager|object
     */
    private $manager;

    /**
     * BadDomainService constructor.
     * @param ContainerInterface $container
     * @param string $entityClass
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        ContainerInterface $container,
        string $entityClass,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct($container, $entityClass);
        $this->setContainer($container);
        $this->manager = $this->getManager();
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param array $data
     * @return BadDomain
     */
    public function create(array $data): BadDomain
    {
        $badDomain = new BadDomain();
        $badDomain->setName($data['name']);

        $this->manager->persist($badDomain);
        $this->manager->flush();

        return $badDomain;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {

        $badDomainsArray = $this->getEntities();

        return $badDomainsArray;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [];
    }
}