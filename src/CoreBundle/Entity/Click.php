<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Click.
 *
 * @ORM\Entity()
 */
class Click
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="string")
     * @ORM\Id
     *
     * @JMS\Expose
     * @JMS\SerializedName("id")
     * @JMS\Type("string")
     */
    protected $id;

    /**
     * @var string
     *
     * @JMS\Type("string")
     *
     * @ORM\Column(name="ua", type="string")
     */
    private $ua;

    /**
     * @var string
     *
     * @JMS\Type("string")
     *
     * @ORM\Column(name="ip", type="string")
     */
    private $ip;

    /**
     * @var string
     *
     * @JMS\Type("string")
     *
     * @ORM\Column(name="ref", type="string")
     */
    private $ref;

    /**
     * @var string
     *
     * @JMS\Type("string")
     *
     * @ORM\Column(name="param1", type="string")
     */
    private $param1;

    /**
     * @var string
     *
     * @JMS\Type("string")
     *
     * @ORM\Column(name="param2", type="string", nullable=true)
     */
    private $param2;

    /**
     * @var integer
     *
     * @JMS\Type("integer")
     *
     * @ORM\Column(name="error", type="integer", options={"default":0})
     */
    private $error;

    /**
     * @var boolean
     *
     * @JMS\Type("boolean")
     *
     * @ORM\Column(name="bad_domain", type="boolean", options={"default":0})
     */
    private $bad_domain;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUa(): string
    {
        return $this->ua;
    }

    /**
     * @param string $ua
     */
    public function setUa(string $ua)
    {
        $this->ua = $ua;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp(string $ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getRef(): string
    {
        return $this->ref;
    }

    /**
     * @param string $ref
     */
    public function setRef(string $ref)
    {
        $this->ref = $ref;
    }

    /**
     * @return string
     */
    public function getParam1(): string
    {
        return $this->param1;
    }

    /**
     * @param string $param1
     */
    public function setParam1(string $param1)
    {
        $this->param1 = $param1;
    }

    /**
     * @return string
     */
    public function getParam2(): string
    {
        return $this->param2;
    }

    /**
     * @param string $param2
     */
    public function setParam2(string $param2)
    {
        $this->param2 = $param2;
    }

    /**
     * @return int
     */
    public function getError(): int
    {
        return $this->error;
    }

    /**
     * @param int $error
     */
    public function setError(int $error)
    {
        $this->error = $error;
    }

    /**
     * @return boolean
     */
    public function isBadDomain(): bool
    {
        return $this->bad_domain;
    }

    /**
     * @param boolean $bad_domain
     */
    public function setBadDomain(bool $bad_domain)
    {
        $this->bad_domain = $bad_domain;
    }

}