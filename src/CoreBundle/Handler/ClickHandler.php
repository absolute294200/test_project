<?php

namespace CoreBundle\Handler;

use CoreBundle\Entity\Click;
use CoreBundle\Service\ClickService;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ClickHandler
 */
class ClickHandler implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $clickService;

    /**
     * MapEventHandler constructor.
     * @param ContainerInterface $container
     * @param EventDispatcherInterface $eventDispatcher
     * @param ClickService $clickService
     */
    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $eventDispatcher,
        ClickService $clickService
    ) {
        $this->setContainer($container);
        $this->eventDispatcher = $eventDispatcher;
        $this->clickService = $clickService;
    }

    /**
     * @param array $data
     * @return Click
     */
    public function processCreate(array $data): Click
    {
        $click = $this->clickService->create($data);

        return $click;
    }

    /**
     * @param string $id
     * @return Click
     * @throws EntityNotFoundException
     */
    public function processGet(string $id) : Click
    {
        $clickArray = $this->clickService->getEntitiesBy(['id' => $id]);

        if(count($clickArray) == 0){
            throw new EntityNotFoundException();
        }

        return $clickArray[0];
    }

    /**
     * @return array
     */
    public function processGetAll() : array
    {
        $clicksArray = $this->clickService->getAll();

        return $clicksArray;
    }

}