<?php

namespace CoreBundle\Handler;

use CoreBundle\Entity\BadDomain;
use CoreBundle\Service\BadDomainService;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Exception\AlreadySubmittedException;

/**
 * Class BadDomainHandler
 */
class BadDomainHandler implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $badDomainService;

    /**
     * BadDomainHandler constructor.
     * @param ContainerInterface $container
     * @param EventDispatcherInterface $eventDispatcher
     * @param BadDomainService $badDomainService
     */
    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $eventDispatcher,
        BadDomainService $badDomainService
    ) {
        $this->setContainer($container);
        $this->eventDispatcher = $eventDispatcher;
        $this->badDomainService = $badDomainService;
    }

    /**
     * @param array $data
     * @return array
     */
    public function processCreate(array $data): array
    {
        $badDomainsArray = $this->processGetAll();
        /** @var BadDomain $item */
        foreach ($badDomainsArray as $item){

            if($item->getName() == $data['name']){
                $data['errorMessage'] = 'The domain already exists';
                throw new AlreadySubmittedException();
            }

        }

        $badDomain = $this->badDomainService->create($data);

        $badDomainsArray[] = $badDomain;

        return $badDomainsArray;
    }

    /**
     * @return array
     */
    public function processGetAll() : array
    {
        $badDomainsArray = $this->badDomainService->getAll();

        return $badDomainsArray;
    }

}